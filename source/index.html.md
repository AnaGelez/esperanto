---
title: Esperanto
---

El [tiu Dropbox](https://www.dropbox.com/sh/rr407n3xva85rv3/AAAfaF7CceJJEExuEs1LzNZ0a?dl=0).

# Affiches/Meme

- [Le cycle de l'eau](/images/watercycle-kids-esperanto-poster.jpg)
- [Prononco de A](/images/prononco-de-a.jpg)
- [Kiu mankas?](/images/kiu-mankas.jpg)
- [Esperanto 2013: Lingva justeco](/images/esperanto-2013-lingva-justeco-blua.jpg)
- [Esperanto 2013: justa komunoakado](/images/esperanto-2013-justa-komunikado.jpg)
- [English is a crazy language](/images/english-is-a-crazy-language.jpg)
- [Bonan Tagon!](/images/bonan-tagon.jpg)
- [Manĝi: esperanta VS angla](/images/manĝi-esperanta-vs-angla.jpg)
- [Tago de la gepatra lingvo](/images/tago-de-la-gepatra-lingvo.jpg)
- [Provu kun Linuks!](/images/provu-kun-linuks.png)
- ["Paco per Esperanto" flago](/images/flago-paco.jpg)
- ["Paco per Esperanto" en 21 lingvoj](/images/paco.jpg)

# Fiche vocab

- [Sur la tablo](/images/sur-la-tablo.png)
- [Vintro](/images/vintro.png)
- [Unuaj verboj](/images/unuaj-verboj.png)
- [Profesioj](/images/profesioj.png)
- [Manĝi](/images/manĝi.jpg)
- [Lerni verbojn](/images/lerni-verbojn.jpg)
- [Legomoj kaj fruktoj](/images/legomoj-kaj-fruktoj.png)
- [Kiel vi fartas?](/images/kiel-vi-fartas.png)
- [Kiel vi sentas?](/images/kia-vi-sentas.jpg)
- [Fruktoj kaj legomoj](/images/fruktoj-kaj-legomoj.png)
- [Frazoj en Esperanto](/images/frazoj-en-esperanto.png)
- [Esperantaj frazoj](/docs/esperantaj-frazoj.pdf)

# Documents

- [Pense-bête en français (PDF, 40 paĝoj)](/docs/vortaro-eo-fr.pdf)
- Listo de senkosta lingvolerna retejo - [PDF](/docs/senkosta-lingvolerna retejo.pdf) - [ODT](/docs/senkosta-lingvolerna retejo.odt)
- Listo de esperanto retejo - [PDF](/docs/retligoj-en-esperanto.pdf) - [ODT](/docs/retligoj-en-esperanto.odt)
- [Radikaro (PDF, 20 paĝoj)](/docs/radikaro.pdf)
- [Esperanta Pakeo (Franca, PDF, 8 paĝoj)](/docs/pakeo-fr.pdf)
- [La tuta Esperanto (PDF, 111 paĝoj)](/docs/la-tuta-esperanto.pdf)
- Lernoplano 2015 franca (Franca, 2 paĝoj) - [PDF](/docs/lernoplano-2015-franca.pdf) - [ODT](/docs/lernoplano-2015-franca.odt)
- [Lernado per retaj kursoj (PDF, 4 paĝoj)](/docs/lernado-per-retaj-kursoj.pdf)
- [France kurso de Esperanto, je la UEA Afrika oficejo (PDF, 79 paĝoj)](/docs/kurso-esperanto-franca.pdf)
- [Kurso de Esperanto (Franca, PDF, 68 paĝoj)](/docs/kurso-de-esperanto.pdf)
- [Konversacio ekzerco (Franca, PDF, 3 paĝoj)](/docs/konversacio-ekzerco.pdf)
- [Intensiva Kurso de Esperanto (PDF, 52 paĝoj)](/docs/intensiva-kurso.pdf)
- [Esperanto, Internet, logiciels libres : des espaces de liberté (Franca, PDF, 2 paĝoj)](/docs/esperanto-internet-logiciels-libres.pdf) 
- [Hejma Vortaro (PDF, 65 paĝoj)](/docs/hejma-vortaro.pdf)
- Frekvencvortaro de Esperanto (41 paĝoj) - [PDF](/docs/frekvencvortaro-de-esperanto.pdf) - [ODT](/docs/frekvencvortaro-de-esperanto.odt)
- [Surstrata kunkantado kun La Kompanoj](esperanto-de-uea-septembro-2015.jpg)
- Esperantaj retejoj (2 paĝoj) - [PDF](/docs/esperantaj-retejoj.pdf) - [ODT](/docs/esperantaj-retejoj.odt)
- [Esperanta Bildvortaro (PDF, 588 paĝoj)](/docs/esperanta-bilvortaro.pdf)
- [Langues et communication : pour une pédagogie des têtes bien faites (Franca, PDF, 2 paĝoj)](/docs/langues-et-communication.pdf)
- [Cours d'Esperanto en dix leçons (Franca, PDF, 53 paĝoj)](/docs/cours-desperanto-en-dix-leçons.pdf)
- [Cours d'Esperanto (Franca, PDF, 53 paĝoj)](/docs/cours-desperanto.pdf)
- [L’espéranto - un joyau éducatif méconnu (Franca, PDF, 5 paĝoj)](/docs/lesperanto-un-joyau-educatif-meconnu.pdf)
- [Esperanto en Tarn-et-Garonne](/docs/esperanto-en-tarn-et-garonne.jpg)
- Apprendre une langue en 7 jours (Franca, 3 paĝoj) - [PDF](/docs/apprendre-une-langue-en-7-jours.pdf) - [ODT](/docs/apprendre-une-langue-en-7-jours.odt)
- [Pense bête afixes](/images/afiksoj.png)
- [Bekkurso 1](/docs/bekkurso-1.pdf)
- [Bekkurso 2](/docs/bekkurso-2.pdf)
- [Bekkurso 3](/docs/bekkurso-3.pdf)
- [Pedagogia Esperanta Kurso 1](/docs/pedagogia-esperanta-kurso-1.doc)
- [Pedagogia Esperanta Kurso 2](/docs/pedagogia-esperanta-kurso-2.doc)
- [Pedagogia Esperanta Kurso 3](/docs/pedagogia-esperanta-kurso-3.doc)
- [Pedagogia Esperanta Kurso 4](/docs/pedagogia-esperanta-kurso-4.doc)
- [Pedagogia Esperanta Kurso 5](/docs/pedagogia-esperanta-kurso-5.doc)
- [Pedagogia Esperanta Kurso 6](/docs/pedagogia-esperanta-kurso-6.doc)
- [Pedagogia Esperanta Kurso 7](/docs/pedagogia-esperanta-kurso-7.doc)
- [Pedagogia Esperanta Kurso 8](/docs/pedagogia-esperanta-kurso-8.doc)
- [Pedagogia Esperanta Kurso 9](/docs/pedagogia-esperanta-kurso-9.doc)
- [Pedagogia Esperanta Kurso 10](/docs/pedagogia-esperanta-kurso-10.doc)
- [Les mots générateurs (Franca, PDF, 2 paĝoj)](/docs/les-mots-générateurs.pdf)
- Commentaires sur l'Esperanto (Franca, 6 paĝoj) - [PDF](/docs/commentaires-sur-lesperanto.pdf) - [ODT](/docs/commentaires-sur-lesperanto.odt)
- [Klarigo pri la Afiŝo de la vortunuoj (PDF, 2 paĝoj)](/docs/klarigo-pri-la-afiŝo-de-la-vortunuoj.pdf)

## Présentations

- Une langue équitable pour communiquer, les langues dans les institutions européennes (Franca, 46 pâgoj) - [PDF](/docs/les-langues-dans-les-institutions-europeenees.pdf) - [PPT](/docs/les-langues-dans-les-institutions-europeenees.ppt)
- [Esperanto rektmetode leciono 17 (PPT, 19 paĝoj)](/docs/esperanto-rektmetode-leciono-17.ppt)
- Cours éclair (Franca, 14 paĝoj) - [PDF](/docs/cours-eclair.pdf) - [ODP](/docs/cours-eclair.odp) - [PPT](/docs/cours-eclair.ppt)
- 10 bonnes raisons d'apprendre l'Esperanto (Franca, 12 paĝoj) - [PDF](/docs/10-bonnes-raisons-dapprendre-lesperanto.pdf) - [PPT](/docs/10-bonnes-raisons-dapprendre-lesperanto.ppt)
- CD Lernu! (Franca, 20 paĝoj) - [PDF](/docs/cd-lernu.pdf) - [PPT](/docs/cd-lernu.ppt)
- Rezolucioj de UNESKO favore de Esperanto - [PDF](/docs/rezolucion-de-unesko-favore-de-esperanto.pdf) - [PPT](/docs/rezolucion-de-unesko-favore-de-esperanto.ppt)
- [The Infancy and Adolescence of Artificial Languages (Angla, , PPT, 60 paĝoj)](/docs/the-infancy-and-adolescence-of-artificial-languages.ppt)
- Esperanto : la langue équitable (Franca, 13 paĝoj) - [PDF](/docs/esperanto-la-langue-equitable.pdf) - [PPT](/docs/esperanto-la-langue-equitable.ppt)
- L'Esperanto pour l'entreprise (Franca, 8 paĝoj) - [PDF](/docs/lesperanto-pour-lentreprise.pdf) - [PPT](/docs/lesperanto-pour-lentreprise.ppt)
- Une langue équitable pour communiquer (Franca, 64 paĝoj) - [PDF](/docs/une-langue-equitable-pour-communiquer.pdf) - [PPT](/docs/une-langue-equitable-pour-communiquer.ppt)
- Geamikoj esperantistaj! (22 paĝoj) - [PDF](/docs/geamikoj-esperantistaj.pdf) - [PPT](/docs/geamikoj-esperantistaj.ppt)
- L’espéranto : Où ? Comment ? Pourquoi ? (Franca, 47 paĝoj) - [PDF](/docs/lesperanto-ou-comment-pourquoi.pdf) - [PPT](/docs/lesperanto-ou-comment-pourquoi.ppt)
- [Esperanto (Sveda, PPT, 38 paĝoj)](/docs/esperanto-sveda.ppt)
- Esperanto, an international artificial language (Angla, 12 paĝoj) - [PDF](/docs/esperanto-an-international-artificial-language.pdf) - [PPT](/docs/esperanto-an-international-artificial-language.ppt)
- L'Esperanto en 30 minutes (Franca, 28 paĝoj) - [PDF](/docs/esperanto-30-minutes.pdf) - [PPT](/docs/esperanto-30-minutes.ppt)
- Rektmetode leciono 1B (15 paĝoj) - [PDF](/docs/rektmetode-leciono-1b.pdf) - [PPT](/docs/rektmetode-leciono-1b.pdf)

# Livres

- [Ĉaro (*B. Traven*, PDF, 510 paĝoj)](/libroj/ĉaro.pdf)
- [Django (*Frans Haacken*, PDF, 19 paĝoj)](/libroj/django.pdf)

# Vidéos

- [France 3 Nord-Pas-de-Calais, 28 julio 2015](/videoj/france-3-nord-pas-de-calais-28-07-2015.mp4)

# Audio

- [Esperanto, Radio Chicoutimi (Franca)](/audio/esperanto-radio-chicoutimi.mp3)
- [Martin STRID rakontas pri Langola skribo al Fransoazo - 1](/audio/martin-strid-rakontas-pri-langola-skribo-al-fransoazo-1.mp3)
- [Martin STRID rakontas pri Langola skribo al Fransoazo - 2](/audio/martin-strid-rakontas-pri-langola-skribo-al-fransoazo-2.mp3)

# Jeux

- [Tabu-ludo](/ludoj/esperanta-tabuo.pdf)
- [Jeux des questions (Franca, PDF, 77 paĝoj](/ludoj/jeu-des-questions.pdf)
- [Farmbienaj bestoj](/ludoj/farmbienaj-bestoj.pdf)
- [Memo](/ludoj/memo.pdf)
- [Fizikaj demandoj](/ludoj/fizikaj-demandoj.odt)

# Liens

- [Plej ofta radikoj en Esperanto](http:/lingvo.org/plejoftaj/)
- [Esperanto Estas](https://www.youtube.com/playlist?list=PL83728C14BFC5822F)
- [Pinterest](https://pinterest.com/pin/345932815104675480/)
- [12 faciloj tekstoj kaj 6 kantoj](http://esperantofre.com/zagreb/zmkanto.htm)
- [Frekvencvortaro de Esperanto](http://slavik.babil.komputilo.org/frekvencvortaro.html)
- [Facilaj rakontoj](http://esperanto.net/literaturo/novel/novlibr/facilverk.html)
